module.exports = {
    // purge: [
	// 	'./public/**/*.html', 
	// 	'./src/**/*.vue',
	// ],
	purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
			colors: {
				primary: '#2DA127',
				secondary: '#13065E',
				border:'#F5F5F5',
				links: '#4895EF',
				darkBlue: '#312963',
				mocha:'#21212187',
				alert:{
					success : '#22D3BB',
					info : '#4895EF',
					warning: '#FFB554',
					error: '#F54952'
				},
				grays: {
					'50':  '#FCFCFC',
					'100': '#F6F6F6',
					'200': '#E5E5E5',
					'300': '#9E9E9E',
					'400': '#707070',
					'500': '#212121',
				},
			},
			fontFamily:{
				pop : "'Poppins', sans-serif",
				'primary': ['Poppins']
			},
			maxWidth: {
				'1024': '1024px'
			},
			maxHeight:{
				'banner':'706px'
			},
			width:{
				'47%' : '47.8571429%',
				'46%' : '65.555556%',
				'23%' : '23.555556%',
				'31%' : '31.333333%'
			}
		},
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
